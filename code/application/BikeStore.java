//Charles-Alexandre Bouchard 2135704
package application;
import vehicles.*;

public class BikeStore {
    public static void main(String[] args){
        Bicycle[] BikeStore = new Bicycle[4];
        BikeStore[0] = new Bicycle("jeff", 12, 14);
        BikeStore[1] = new Bicycle("jay", 10, 25);
        BikeStore[2] = new Bicycle("john", 9, 17);
        BikeStore[3] = new Bicycle("joe", 11, 27);

        for(int i = 0; i < BikeStore.length; i++){
            System.out.println(BikeStore[i]);
        }
    }
}
